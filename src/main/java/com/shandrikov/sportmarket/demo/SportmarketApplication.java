package com.shandrikov.sportmarket.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportmarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportmarketApplication.class, args);
    }



 }
